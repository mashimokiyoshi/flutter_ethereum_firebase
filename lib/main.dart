import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart' as firestore;
import 'package:velocity_x/velocity_x.dart';
import 'package:web3dart/web3dart.dart';
import 'package:http/http.dart';

Future<void> main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<FirebaseUser> _handleSignIn() async {
    GoogleSignInAccount googleCurrentUser = _googleSignIn.currentUser;
    try {
      if (googleCurrentUser == null) googleCurrentUser = await _googleSignIn.signInSilently();
      if (googleCurrentUser == null) googleCurrentUser = await _googleSignIn.signIn();
      if (googleCurrentUser == null) return null;

      GoogleSignInAuthentication googleAuth = await googleCurrentUser.authentication;
      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      final FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
      print("signed in " + user.displayName);

      return user;
    } catch (e) {
      print(e);
      return null;
    }
  }

  void transitionNextPage(FirebaseUser user) {
    if (user == null) return;

    Navigator.push(context, MaterialPageRoute(builder: (context) =>
        NextPage(userData: user)
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                      child: Text(
                        "Hello",
                        style: TextStyle(
                          fontSize: 80.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(15.0, 65.0, 0.0, 0.0),
                      child: Text(
                        "There",
                        style: TextStyle(
                            fontSize: 80.0,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(240.0, 65.0, 0.0, 0.0),
                      child: Text(
                        ".",
                        style: TextStyle(
                            fontSize: 80.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.green
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 35.0, left: 20.0, right: 20.0),
                child: Column(
                  children: [
                    ButtonTheme(
                      minWidth: 300.0,
                      child: RaisedButton(
                        onPressed: () {
                          _handleSignIn()
                              .then((FirebaseUser user) =>
                              transitionNextPage(user)
                          )
                              .catchError((e) => print(e));
                        },
                        textColor: Colors.white,
                        shape: const StadiumBorder(),
                        color: Colors.red,
                        child: Text('Sign in Google'),
                      ),
                    ),
                  ],
                ),
              ),
            ]
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class NextPage extends StatefulWidget {
  FirebaseUser userData;

  NextPage({Key key, this.userData}) : super(key: key);

  @override
  _NextPageState createState() => _NextPageState(userData);
}

class _NextPageState extends State<NextPage> {
  Client httpClient;
  Web3Client ethClient;
  String lastTransactionHash;
  final String contractAddress = '';

  FirebaseUser userData;
  String name;
  String displayName = '';
  String email;
  String photoUrl;
  String uid;
  String myAddress = '';
  String privateKey;
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  List<firestore.DocumentSnapshot> documentList = [];
  String selectUser = '送信先を選択';
  String selectUserAddress = '';


  int myAmount = 0;
  var _navIndex = 0;

  @override
  void initState() {
    super.initState();
    httpClient = new Client();
    ethClient = new Web3Client("http://localhost:7545", httpClient);
  }

  _NextPageState(FirebaseUser userData) {
    this.userData = userData;
    this.name = userData.displayName;
    this.email = userData.email;
    this.photoUrl = userData.photoUrl;
    this.uid = userData.uid;
    this.privateKey = sha256.convert(utf8.encode(userData.uid + 'flutter-ethereum-firebase')).toString();
    setUserInfo();
    //registerAddress();
  }

  Future<void> setUserInfo() async {
    final document = await firestore.Firestore.instance
        .collection('users')
        .document(this.uid)
        .get();
    if (document.exists) {
      setState(() {
        this.displayName = '${document['name']}';
        this.myAddress = '${document['address']}';
      });
    } else {
      await EthPrivateKey.fromHex(this.privateKey)
          .extractAddress()
          .then((value) =>
              this.myAddress = value.toString());
              await firestore.Firestore.instance
                  .collection('users')
                  .document(this.uid)
                  .setData({'name': this.name, 'address': this.myAddress});

      setState(() {
        this.displayName = this.name;
        this.myAddress = this.myAddress;
      });
    }
  }

  Future<void> registerAddress() async {
    var myAddress = await EthPrivateKey.fromHex(this.privateKey).extractAddress();
    this.myAddress = myAddress.toString();
    setState(() => this.myAddress);
  }

  Future<void> _handleSignOut() async {
    await FirebaseAuth.instance.signOut();
    try {
      await _googleSignIn.signOut();
    } catch (e) {
      print(e);
    }
    Navigator.pop(context);
  }

  Future<String> sendCoind(String targetAddressHex, int amount) async {
    EthereumAddress address = EthereumAddress.fromHex(targetAddressHex);
    // unit in smart contract means BigInt for us
    var bigAmount = BigInt.from(amount);
    // sendCoin transaction
    var response = await submit("sendCoin", [address, bigAmount]);
    // hash of the transaction
    return response;
  }

  Future<String> submit(String functionName, List<dynamic> args) async {
    EthPrivateKey credentials = EthPrivateKey.fromHex(this.privateKey);

    DeployedContract contract = await loadContract();

    final ethFunction = contract.function(functionName);

    var result = await ethClient.sendTransaction(
      credentials,
      Transaction.callContract(
        contract: contract,
        function: ethFunction,
        parameters: args,
      ),
    );
    return result;
  }

  Future<List<dynamic>> getBalance(String targetAddressHex) async {
    EthereumAddress address = EthereumAddress.fromHex(targetAddressHex);
    List<dynamic> result = await query("getBalance", [address]);
    return result;
  }

  Future<List<dynamic>> query(String functionName, List<dynamic> args) async {
    final contract = await loadContract();
    final ethFunction = contract.function(functionName);
    final data = await ethClient.call(
        contract: contract, function: ethFunction, params: args);
    return data;
  }

  Future<DeployedContract> loadContract() async {
    String abiCode = await rootBundle.loadString("assets/abi.json");
    String contractAddress = this.contractAddress;

    final contract = DeployedContract(ContractAbi.fromJson(abiCode, "MetaCoin"),
        EthereumAddress.fromHex(contractAddress));
    return contract;
  }

  void _onButtonPressed() {
    showModalBottomSheet(context: context, builder: (context) {
      return Container(
        color: Color(0xFF737373),
        height: 900,
        child: Container(
          child: _buildBottomNavigationMenu(),
          decoration: BoxDecoration(
            color: Theme.of(context).canvasColor,
            borderRadius: BorderRadius.only(
              topLeft: const Radius.circular(10),
              topRight: const Radius.circular(10),
            )
          ),
        ),
      );
    });
  }

  Column _buildBottomNavigationMenu() {
    return Column(
        children: documentList.map((document) {
          return ListTile(
            title: Text('${document['name']}さん'),
            onTap: () => setState(() {
              this.selectUser = document['name'];
              this.selectUserAddress = document['address'];
              Navigator.of(context).pop();
            }),
          );
        }).toList(),
    );
  }





  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body:
        ZStack([
          VxBox()
            .red600
            .size(context.screenWidth, context.percentHeight*33)
            .make(),
          VStack([
            (context.percentHeight*10).heightBox,
            (this.displayName + ' さん').text.xl3.white.bold.makeCentered().py16(),
            (context.percentHeight*5).heightBox,
            VxBox(
              child: VStack([
                "コイン保有数".text.gray700.xl2.semiBold.makeCentered(),
                20.heightBox,
                FutureBuilder(
                  future: getBalance(this.myAddress),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Text(
                        '${snapshot.data[0]}C',
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 50),
                      ).centered().shimmer(primaryColor: Colors.black87);
                    } else
                      return CircularProgressIndicator().centered();
                  },
                ),
              ]),
            )
              .p16
              .white
              .size(context.screenWidth, context.percentHeight*18)
              .rounded
              .shadowXl
              .make()
              .p16(),
            30.heightBox,
            Container(
              padding: const EdgeInsets.all(5.0),
              width: 400,
              height: 330,
              child: Card(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                  side: BorderSide(width: 2, color: Colors.black12, style: BorderStyle.solid),

                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      '送金',
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                    ),
                    Container(
                      width: 370,
                      height: 90,
                      padding: const EdgeInsets.all(5.0),
                      child: new InkWell(
                        onTap: () async {
                          // コレクション内のドキュメント一覧を取得
                          final snapshot = await firestore.Firestore.instance.collection('users').getDocuments();
                          // 取得したドキュメント一覧をUIに反映
                          setState(() {
                            documentList = snapshot.documents;
                          });
                          _onButtonPressed();
                        },
                        child: Card(
                          color: Colors.white,
                          margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                            side: BorderSide(width: 2, color: Colors.blue),
                          ),
                          child: ListTile(
                            leading: Icon(
                              Icons.account_circle,
                              color: Colors.black,
                            ),
                            title: Text(
                              this.selectUser,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 20.0
                              ),
                            ),
                          ),
                        ),
                      )
                    ),
                    Container(
                      width: 330,
                      height: 60,
                      padding: const EdgeInsets.all(5.0),
                      child: RaisedButton(
                        child: Text(
                          '10C送金',
                          style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white
                          ),
                        ),
                        color: Colors.blue,
                        shape: const StadiumBorder(),
                        onPressed: () async {
                          var result = await sendCoind(
                              this.selectUserAddress, 10);
                        },
                      ),
                    ),
                  ],
                ),
              )
            ).centered(),
            /*HStack(
              [
                FlatButton.icon(
                  onPressed: () async {
                    // コレクション内のドキュメント一覧を取得
                    final snapshot = await firestore.Firestore.instance.collection('users').getDocuments();
                    // 取得したドキュメント一覧をUIに反映
                    setState(() {
                      documentList = snapshot.documents;
                    });
                    _onButtonPressed();

                  },
                  //onPressed: () async {
                  //  var result = await sendCoind(
                  //      "0x54adb3db951f32fa53786c8ef3d750f2053009c9", 10);
                  //},
                  color: Colors.blue,
                  shape: Vx.roundedSm,
                  icon: Icon(
                    Icons.call_made,
                    color: Colors.white,
                  ),
                  label: "テスト さんへ 10p 送信".text.white.make(),
                ).h(50),
              ],
              alignment: MainAxisAlignment.spaceAround,
              axisSize: MainAxisSize.max,
            ).p16()*/
          ]),
        ]),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
            backgroundColor:  Colors.blue
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.list),
              title: Text('History'),
              backgroundColor:  Colors.blue
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Text('Profile'),
              backgroundColor:  Colors.blue
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.exit_to_app),
              title: Text('Exit'),
              backgroundColor:  Colors.blue,
          ),
        ],
        onTap: (int index) {
          if (index==3) {
            _handleSignOut().catchError((e) => print(e));
          }
          setState(
            () {
              _navIndex = index;
            },
          );
        },
        currentIndex: _navIndex,
      ),
    );
  }
}